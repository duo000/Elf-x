package common{
	public class ArrayUtil {
		public static function indexOfField(a:Array, v:*, f:String = "id"):int {
			if(!a) return -1;
			for(var i:int = 0;i < a.length;i++){
				if(a[i][f] == v) return i;
			}
			return -1;
		}
		public static function itemOfField(a:Array, v:*, f:String = "id"):Object {
			if(!a) return -1;
			for(var i:int = 0;i < a.length;i++){
				if(a[i][f] == v) return a[i];
			}
			return null;
		}
	}
}