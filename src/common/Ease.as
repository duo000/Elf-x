package common{
	public class Ease{
		public static function none(t:Number, b:Number, c:Number, d:Number):Number{
			return c * t / d + b;
		}
		public static function strongIn(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d;
			return c * t * t * t * t * t + b;
		}
		public static function strongOut(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d - 1;
			return c * (t * t * t * t * t + 1) + b;
		}
		public static function strongInOut(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d / 2;
			if (t<1) return c / 2 * t * t * t * t * t + b;
			t = t - 2;
			return c / 2 * (t * t * t * t * t + 2) + b;
		}
		public static function backIn(t:Number, b:Number, c:Number, d:Number, s:Number = 1.70158):Number {
			t = t / d;
			return c * t * t * ((s + 1) * t - s) + b;
		}
		public static function backOut(t:Number, b:Number, c:Number, d:Number, s:Number = 1.70158):Number {
			t = t / d - 1;
			return c * (t * t * ((s + 1) * t + s) + 1) + b;
		}
		public static function backInOut(t:Number, b:Number, c:Number, d:Number, s:Number = 1.70158):Number {
			t = t / d / 2;
			if (t<1){
				s = s * (1.525);
				return c / 2 * (t * t * ((s + 1) * t - s)) + b;
			}
			t = t - 2;
			s = s * (1.525);
			return c / 2 * (t * t * ((s + 1) * t + s) + 2) + b;
		}
		public static function circIn(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d;
			return - c * (Math.sqrt(1 - t * t) - 1) + b;
		}
		public static function circOut(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d - 1;
			return c * Math.sqrt(1 - t * t) + b;
		}
		public static function circInOut(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d / 2;
			if (t<1) return - c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
			t = t - 2;
			return c / 2 * (Math.sqrt(1 - t * t) + 1) + b;
		}
		public static function elasticIn(t:Number, b:Number, c:Number, d:Number, a:Number = 0, p:Number = 0):Number {
			var s:Number;
			if (t == 0) return b;
			t = t / d;
			if (t==1)  return b+c
			if (!p) p=d*.3
			if (! a || a<Math.abs(c)){
				a = c;
				s = p / 4;
			}
			else{
				s = p / (Math.PI*2) * Math.asin(c / a);
			}
			t = t - 1;
			return -(a * Math.pow(2, 10 * t) * Math.sin((t * d - s) * Math.PI*2 / p)) + b;
		}
		public static function elasticOut(t:Number, b:Number, c:Number, d:Number, a:Number = 0, p:Number = 0):Number {
			var s:Number;
			if (t == 0)  return b;
			t = t / d;
			if (t == 1) return b + c;
			if (!p) p=d*0.3
			if (!a || a<Math.abs(c)){
				a = c;
				s = p / 4;
			}
			else{
				s = p / (Math.PI*2) * Math.asin(c / a);
			}
			return(a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * Math.PI*2 / p) + c + b);
		}
		public static function elasticInOut(t:Number, b:Number, c:Number, d:Number, a:Number = 0, p:Number = 0):Number {
			var s:Number;
			if (t == 0) return b;
			t = t / d / 2;
			if (t == 2) return b + c;
			if (!p) p = d * (.3 * 1.5);
			if (!a || a<Math.abs(c)){
				a = c;
				s = p / 4;
			}
			else{
				s = p / (Math.PI*2) * Math.asin(c / a);
			}
			if (t<1){
				t = t - 1;
				return -0.5 * (a * Math.pow(2, 10 * t) * Math.sin((t * d - s) * Math.PI*2 / p)) + b;
			}
			t = t - 1;
			return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * Math.PI*2 / p) * 0.5 + c + b;
		}
		public static function quadIn(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d;
			return c * t * t + b;
		}
		public static function quadOut(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d;
			return -c * t * (t - 2) + b;
		}
		public static function quadInOut(t:Number, b:Number, c:Number, d:Number):Number {
			t = t / d * 0.5;
			if(t < 1) return c * 0.5 * t * t + b;
			t = t - 1;
			return -c * 0.5 * (t * (t - 2) - 1) + b;
		}
	}

}