﻿package components{
	import core.Element;
	public class TextInput extends Element{
		public var font:String;
		public var size:int;
		public var color:int;
		public var displayAsPassword:Boolean;
		public var maxLength:int;
		public var text:String;
		public function TextInput(symbol:String=null,style:Object=null){}
		public function setFont(v:String):void{}
		public function setFontSize(v:int):void{}
		public function setColor(v:int):void{}
		public function setDisplayAsPassword(v:Boolean):void{}
		public function setMaxLength(v:int):void{}
		public function setText(v:String):void{}
	}
}