﻿package components{
	import core.Clip;
	public class Animate extends Clip{
		public var duration:int;
		public var loop:Boolean;
		public var frames:Array;
		public var frameIndex:int;
		public var runOnFrame:Array;
		public var runOnEnd:Array;
		public var isPlaying:Boolean;
		public function Animate(symbol:String = null, style:Object = null){}
		public function setFrame(startIndex:int, endIndex:int):void { }
		public function stop():void{}
		public function play():void{}
	}
}