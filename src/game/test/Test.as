﻿package game.test{
	import components.Animate;
	import core.App;
	import core.Clip;
	import ui.PTest;
	public class Test extends PTest {
		public var animate:Animate;
		private var clip:Clip;
		public function Test() {
			this.setEnabled(true);
			App.setPage(this);
			this.createUI(PTest.mainUI);
			var clip:Clip = new Clip("world.101.0");
			clip.setCenterPos(0, 0);
			this.addChild(clip);
			this.clip = clip;
		}
		[EventListener(event="click", dispatcher="")]
		public function onClick():void{
			this.clip.reset("world.101.6");
			trace(8);
		}
	}
}