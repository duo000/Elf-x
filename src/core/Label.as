﻿package core{
	public class Label extends Element{
		public var font:String;
		public var size:int;
		public var color:int;
		public var align:String;
		public var leading:int;
		public var letterSpacing:int;
		public var wordWrap:Boolean;
		public var textWidth:int;
		public var textHeight:int;
		public var text:String;
		public function Label(symbol:String=null,style:Object=null){}
		public function setFont(v:String):void{}
		public function setFontSize(v:int):void{}
		public function setColor(v:int):void{}
		public function setAlign(v:String):void{}
		public function setLeading(v:int):void{}
		public function setLetterSpacing(v:int):void{}
		public function setWordWrap(v:Boolean):void{}
		public function setText(v:String):void{}
	}
}