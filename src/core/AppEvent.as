﻿package core{
	public class AppEvent{
		public static var PRESS:String = "press";
		public static var RELEASE:String = "release";
		public static var CLICK:String = "click";
		public static var MOVE:String = "move";
		public static var BUTTON_CLICK:String = "buttonClick";
		public static var ENTER_FRAME:String = "enterFrame";
		public static var RESPONSE:String = "response";
		public var type:String;
		public var name:String;
		public var target:Object;
		public var data:Object;
		public var x:int;
		public var y:int;
		public function AppEvent(type:String){}
	}
}