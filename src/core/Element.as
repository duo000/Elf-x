﻿package core{
	public class Element{
		public var className:String;
		public var id:int;
		public var symbol:String;
		public var name:String;
		public var style:Object;
		public var x:int;
		public var y:int;
		public var z:int;
		public var z2:int;
		public var parent:Element;
		public var numChildren:int;
		public var width:int;
		public var height:int;
		public var scaleX:Number;
		public var scaleY:Number;
		public var rotation:int;
		public var visible:Boolean;
		public var alpha:Number;
		public var enabled:Boolean;
		public var hitBounds:Array;
		public function Element(symbol:String=null,style:Object=null){}
		public function contains(target:Element):Boolean{return false;}
		public function bindStageListener():void{}
		public function removeStageListener():void{}
		public function remove():void{}
		public function removeChildren():void{}
		public function addChild(child:Element):Element{return null;}
		public function addChildAt(child:Element,index:int):Element{return null;}
		public function getChildAt(index:int):Element{return null;}
		public function getChildById(id:int):Element{return null;}
		public function getChildByName(name:String):Element{return null;}
		public function getBox(name:String):Box{return null;}
		public function setPosition(x:int,y:int):void{}
		public function setSize(w:int,h:int):void{}
		public function localToGlobal(x:int=0,y:int=0):Array{return null;}
		public function globalToLocal(x:int=0,y:int=0):Array{return null;}
		public function hitTest(x:int,y:int):Boolean{return false;}
		public function dispatchEvent(evt:AppEvent):void{}
		public function getStage():Stage{return null;}
		public function setX(v:int):void{}
		public function setY(v:int):void{}
		public function setZ(z:int,z2:int=0):void{}
		public function setScaleX(v:Number):void{}
		public function setScaleY(v:Number):void{}
		public function setScale(scaleX:Number,scaleY:Number=NaN):void{}
		public function setRotation(v:int):void{}
		public function setWidth(v:int):void{}
		public function setHeight(v:int):void{}
		public function setVisible(v:Boolean):void{}
		public function setAlpha(v:Number):void{}
		public function setEnabled(v:Boolean):void{}
		public function setHitBounds(v:Array):void{}
		public function toString():String{return null;}
	}
}