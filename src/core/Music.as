package core{
	public class Music {
		public static function play(url:String):void{}
		public static function stop():void{}
		public static function pause():void{}
		public static function resume():void{}
		public static function isPlaying():Boolean{return false;}
		public static function playSound(url:String):void{}
	}
}