﻿package core{
	public class App{
		public static var baseW:int;
		public static var baseH:int;
		public static var width:int;
		public static var height:int;
		public static var styles:Object;
		public static var stage:Stage;
		public static var root:Box;
		public static var isPress:Boolean;
		public static var pressElementX:int;
		public static var pressElementY:int;
		public static var pressX:int;
		public static var pressY:int;
		public static var moveX:int;
		public static var moveY:int;
		public static function initialize():void{}
		public static function doTimeout(owner:Object, func:Function, delay:int = 1, ...args):int{return 0;}
		public static function doInterval(owner:Object, func:Function, delay:int = 1, ...args):int{return 0;}
		public static function clearTimer(timerId:int):void{}
		public static function exec(foo:Array):*{return null;}
		public static function formatColor(color:int):int{ return 0;}
		public static function formatArray(arr:*):Array{return null;}
		public static function setPage(page:Box):void{}
		public static function isClass(instance:Object, classObj:Class):Boolean{return false;}
		public static function removeTempTexture():void{}
		public static function bindStyle(symbol:String,f:String,v:*):void{}
		public static function setShareData(f:String,v:*):void{}
		public static function getShareData(f:String):String{return null;}
		public static function eval(code:String):*{return null;}
	}
}