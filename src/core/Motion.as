package core{
	public class Motion {
		public var timerId:int;
		public var step:int;
		public var frame:int;
		public var frameMax:int;
		public var from:Number;
		public var to:Number;
		public var value:Number;
		public var a:Array;
		public function Motion(owner:Object,func:Function,...args){}
		public function stop():void {}
		public function nextFrame():void {}
		public function nextStep():void {}
	}
}