﻿package core{
	public class Clip extends Element{
		public var index:int;
		public var flip:Boolean;
		public var baseW:int;
		public var baseH:int;
		public var centerPos:Array;
		public var useTempTexture:Boolean;
		public function Clip(symbol:String=null,style:Object=null){}
		public function reset(symbol:String,index:int=0,w:int=-1,h:int=-1):void{}
		public function setIndex(v:int):void{}
		public function setFlip(v:Boolean):void{}
		public function setCenterPos(x:int,y:int):void{}
		public function setScrollRect(x:int, y:int, w:int, h:int):void{}
		public function setColor(v:int):void{}
	}
}