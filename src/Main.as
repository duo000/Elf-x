﻿package {
	import components.Animate;
	import core.Box;
	import core.Clip;
	import core.Http;
	import game.test.Test;
	public class Main extends Box {
		
		public static var notBuildAsset:Boolean = false;//不编译图片
		public function start():void {
			//new Test();
			var animate:Animate = new Animate("world.101");
			this.addChild(animate);
			animate.play();
			animate.setPosition(200, 200);
		}
	}
}