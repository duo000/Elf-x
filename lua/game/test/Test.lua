local Test=class(Box,{eventListener={}})
function Test:ctor()
	self:setEnabled(true)
	App.setPage(self)
	self:createUI(PTest.mainUI)
	local clip=Clip.new("world.101.0")
	clip:setCenterPos(0,0)
	self:addChild(clip)
	self.clip=clip
end
Test.eventListener.onClick={event="click",dispatcher=""}
function Test:onClick()
	self.clip:reset("world.101.6")
	trace(8)
end
return Test