local Animate=class(Clip)
function Animate:ctor(symbol,style)
	self.duration=self.style.duration or 100
	self.loop=self.style.loop
	if self.loop==nil then self.loop=true end
	self.timerId=0
	self.isPlaying=false
	self:reset(symbol)
	if self.style.autoPlay then self:play() end
end
function Animate:setFrame(startIndex,endIndex)
	self.frames=Array.new()
	for i=startIndex,endIndex do
		self.frames:push(i)
	end
end
function Animate:reset(symbol)
	if symbol then self:resetClip(symbol) end
	if not self.symbol then return end
	local startIndex=1
	local endIndex=startIndex
	if self.cropMode then
		while App.cropItems[symbol.."."..(endIndex+1)] do
			endIndex=endIndex+1
		end
	else 
		endIndex=self.colNum*self.rowNum
	end
	self:setFrame(startIndex,endIndex)
end
function Animate:stop()
	self.isPlaying=false
	App.clearTimer(self.timerId)
end
function Animate:play()
	self:stop()
	self.isPlaying=true
	self.frameIndex=0
	self:onEnterFrame()
	self.timerId=App.doInterval(self,self.onEnterFrame,self.duration)
end
function Animate:onEnterFrame()
	if self:getStage()==nil then
		self:stop()
		return
	end
	self:resetClip(self.symbol,self.frames[self.frameIndex]-1)
	App.exec(self.runOnFrame)
	self.frameIndex=self.frameIndex+1
	if self.frameIndex>=self.frames.length then
		self.frameIndex=0
		if self.loop==false then
			self:stop()
		end
		App.exec(self.runOnEnd)
	end
end

return Animate