local TextInput=class(Element)
function TextInput:ctor(symbol,style)
	self.className="TextInput"
	self.font="SimHei"
	self.size=36
	self.color=0xffffff
	self.displayAsPassword=false
	self.maxLength=0
	self.text=""
	local bg=CCScale9Sprite:create("assets/components/TextInput.png")
	local w=200
	local h=42
	if self.style.width then w=Math.floor(self.style.width) end
	if self.style.height then h=Math.floor(self.style.height) end
	self.ccInput=CCEditBox:create(CCSize(w,h), bg)
	self.ccInput:setAnchorPoint(CCPoint(0,1))
	self.ccNode:addChild(self.ccInput)
	if self.style.font then self:setFont(self.style.font) end
	if self.style.size then self:setFontSize(self.style.size) end
	if self.style.color then self:setColor(self.style.color) end
	if self.style.displayAsPassword then self:setDisplayAsPassword(self.style.displayAsPassword) end
	if self.style.maxLength then self:setMaxLength(self.style.maxLength) end
	if self.style.text then self:setText(self.style.text) end
end
function TextInput:setFont(v)
	self.font=v
	self.ccInput:setFontName(v)
end
function TextInput:setFontSize(v)
	self.size=v
	self.ccInput:setFontSize(v)
end
function TextInput:setColor(v)
	self.color=v
	self.ccInput:setFontColor(App.formatColor(v))
end
function TextInput:setDisplayAsPassword(v)
	self.displayAsPassword=v
	self.ccInput:setInputFlag(kEditBoxInputFlagPassword)
end
function TextInput:setMaxLength(v)
	self.maxLength=v
	self.ccInput:setMaxLength(v)
end
function TextInput:setText(v)
	self.text=v
	self.ccInput:setText(v)
end
function TextInput:getText()
	self.ccInput:getText()
end
function TextInput:setSize(w,h)
	self.width=w
	self.height=h
	self.ccInput:setContentSize(CCSize(w,h))
end

return TextInput