local RNews={}
function RNews.getItem(id)
	RNews.getAll()
	for i=0,RNews.items.length do
		local item=RNews.items[i]
		if item.id==id then return item end
	end
end
function RNews.getAll()
	if RNews.items~=nil then return RNews.items end
	RNews.items={
		["zero"]={["id"]=1,["name"]="酒醉摔伤",["desc"]="酒醉摔伤，住院治疗2天"},
		{["id"]=2,["name"]="食物中毒",["desc"]="食物中毒，住院治疗3天"},
		{["id"]=3,["name"]="被马踢伤",["desc"]="被马踢伤，住院治疗4天"},
		{["id"]=4,["name"]="殴打交警",["desc"]="殴打交警，被依法拘留2天"},
		{["id"]=5,["name"]="购买贼赃",["desc"]="购买贼赃，被依法拘留3天"},
		{["id"]=6,["name"]="商业贿赂",["desc"]="商业贿赂，被依法拘留4天"},
		{["id"]=7,["name"]="走私毒品",["desc"]="走私毒品，住院治疗5天"},
		{["id"]=8,["name"]="检到钱包",["desc"]="在路边检到钱包，吞为已有，获得1000元"},
		{["id"]=9,["name"]="获得遗产",["desc"]="意外获得神秘遗产5000元"},
		{["id"]=10,["name"]="侵入银行",["desc"]="进入银行电脑，提取其他玩家10%存款"},
		{["id"]=11,["name"]="外星绑架",["desc"]="被外星人绑架3天"},
		{["id"]=12,["name"]="时空隧道",["desc"]="遇见时空隧道，被传送到地图的随机点"},
		{["id"]=13,["name"]="神仙送礼",["desc"]="过节了，神仙给大家送惊喜了"},
		{["id"]=14,["name"]="台风过境",["desc"]="台风过境，摧毁房屋一幢"},
		{["id"]=15,["name"]="怪兽袭击",["desc"]="怪兽袭击，摧毁房屋一幢"},
		{["id"]=16,["name"]="外星袭击",["desc"]="外星人袭击，摧毁房屋一幢"},
		{["id"]=17,["name"]="病患出院",["desc"]="住院中病患提前出院"},
		{["id"]=18,["name"]="犯人大赦",["desc"]="监狱中犯人大赦提前释放"}
	}
	RNews.items[0]=RNews.items.zero
	RNews.items.length=#RNews.items+1
	return RNews.items
end
return RNews