local Main=class(Box,{eventListener={},notBuildAsset=true})
function Main:_define()
end

Main.notBuildAsset=false;
function Main:start()
	local animate=Animate.new("world.101")
	self:addChild(animate)
	animate:play()
	animate:setPosition(200,200)
	animate.ccNode:setCascadeOpacityEnabled(true)
	Transition.fadeTo(animate.ccNode,{opacity  = 128, time = 1.5})
end
return Main