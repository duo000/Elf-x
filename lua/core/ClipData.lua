local ClipData = {}
function ClipData.getTexture(symbol,useTempTexture)
	local url="assets/"..(_G["string"]["gsub"](symbol,"%.","/"))..".png"
	local textureCache=_G["CCTextureCache"]["sharedTextureCache"](_G["CCTextureCache"])
	local texture=textureCache:textureForKey(url)
	if not texture then
		texture=textureCache["addImage"](textureCache,url)
		if useTempTexture then
			App.tempTextures:push(texture)
		end
	end
	return texture
end
function ClipData.setDefaultGrid(symbol,col,row)
	if symbol==nil then return end
	if App.styles[symbol]==nil then App.styles[symbol]={} end
	local style=App.styles[symbol]
	if style.clipSize==nil then style.clipSize=Array.new(Array.new(col,row)) end
end

return ClipData