local Box=class(Element)
function Box:ctor(symbol,style)
	self.className="Box"
	self.isBox=true
end
function Box:createUI(ui)
	local builds={}
	local root=self
	if ui.className~="" and ui.className~=nil then
		local elementClass=_G[ui.className]
		root=elementClass.new(nil,self:getStyleByNode(ui))
		self:addChild(root)
	end
	local queues={{ui,root}}
	while #queues>0 do
		local a=queues[#queues]
		_G["table"]["remove"](queues,#queues)
		local nodes=a[1]
		local box=a[2]
		for i=1,#nodes do
			local node=nodes[i]
			local style=self:getStyleByNode(node)
			local elementClass=_G[style.className]
			local element=elementClass.new(nil,style)
			box:addChild(element)
			if element.isBox then
				_G["table"]["insert"](queues,1,{node,element})
			end
			if node.elementVar then
				self[node.elementVar]=element
			end
			_G["table"]["insert"](builds,1,{node,element})
		end
	end
	return root
end
function Box:bindData(...)
	local args={...}
	local queues={self.ccNode}
	while #queues>0 do
		local ccNode=queues[#queues]
		_G["table"]["remove"](queues,#queues)
		local ccArr=ccNode["getChildren"](ccNode)
		local count=ccArr and ccArr["count"](ccArr) or 0
		for i=0,count-1 do
			local ccObj=ccArr["objectAtIndex"](ccArr,i)
			local ccNode2=_G["tolua"]["cast"](ccObj,"CCNode")
			local element=ccNode2.bindObj
			if not element then goto next end
			if element.isBox then
				_G["table"]["insert"](queues,1,ccNode2)
			end
			if not element.paramExpr then goto next end
			local text=element.paramExpr
			local count2=App.toInt(#args/2)
			for j=0,count2-1 do
				local p=args[j*2+1]
				local v=args[j*2+2]
				text=String.replace(text,"{"..p.."}",v)
			end
			if not _G["string"]["find"](text,"{") then
				element:setParam(text)
			end
			::next::
		end
	end
end
function Box:getStyleByNode(node)
	local style={}
	for k in pairs(node) do
		local v=node[k]
		if v=="true" then v=true end
		if v=="false" then v=false end
		style[k]=v
	end
	local baseStyle=App.styles[style.symbol]
	if baseStyle and baseStyle.extendsClass and style.extendsClass==nil then
		style.extendsClass=baseStyle.extendsClass
	end
	if style.extendsClass then style.className=style.extendsClass end
	if style.relative then self:resizeForScreen(style) end
	return style
end
function Box:resizeForScreen(style)
	local relative=style.relative
	local dx=0
	local dy=0
	local dw=0
	local dh=0
	if relative=="right" or relative=="rightBottom" or relative=="rightMiddle" or relative=="rightStretch" then dx=App.width-App.baseW end
	if relative=="bottom" or relative=="rightBottom" or relative=="bottomCenter" or relative=="bottomStretch" then dy=App.height-App.baseH end
	if relative=="center" or relative=="centerMiddle" or relative=="bottomCenter" then dx=(App.width-App.baseW)/2 end
	if relative=="middle" or relative=="rightMiddle" or relative=="centerMiddle" then dy=(App.height-App.baseH)/2 end
	if relative=="stretchWidth" or relative=="stretchBoth" or relative=="bottomStretch" then dw=App.width-App.baseW end
	if relative=="stretchHeight" or relative=="stretchBoth" or relative=="rightStretch" then dh=App.height-App.baseH end
	
	style.x=App.toInt(style.x)+dx
	style.y=App.toInt(style.y)-dy
	style.width=App.toInt(style.width)
	style.height=App.toInt(style.height)
	if style.className=="Clip" and style.width>0 and style.height>0 and (dw>0 or dh>0) then
		if style.clip9Grid then
			style.width=dw+style.width
			style.height=dh+style.height
		else
			style.scaleX=(dw+style.width)/style.width
			style.scaleY=(dh+style.height)/style.height
		end
	end
end
function Box:sortDepth()
	if sortTouchEvent==nil then sortTouchEvent=false end
	local a=self.children
	local count = a.length -1
	while count > 0 do
		local k = 0
		for i=0,count-1 do
			local c1=a[i]
			local c2=a[i+1]
			local swap=false
			if c1.z>c2.z then swap=true end
			if c1.z==c2.z and c1.y>c2.y then swap=true end
			if c1.z==c2.z and c1.y==c2.y and c1.x>c2.x then swap=true end
			if c1.z==c2.z and c1.y==c2.y and c1.x==c2.x and c1.z2>c2.z2 then swap=true end
			if swap then 
				local t = a[i]
				a[i] = a[i + 1]
				a[i + 1] = t
				k = i 
			end 
		end 
		count = k
	end
	
	for i=0,a.length-1 do
		local child=a[i]
		self.ccNode["reorderChild"](self.ccNode,child.ccNode,i)
	end
end
function Box:sortTouchEvent()
	local a=self.children
	for i=0,a.length-1 do
		local child=a[i]
		if child.enabled then
			child.enabled=false
			child:setEnabled(true)
		end
	end
end
return Box