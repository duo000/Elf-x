local Stage=class(Box)
function Stage:ctor(symbol,style)
	self.className="Stage"
	self.enabled=true
	self.stageListeners=Array.new()
end
function Stage:addPageListener(page)
	for k in pairs(page.eventListener) do
		local listener=page.eventListener[k]
		if listener.dispatcher=="stage" then
			self.stageListeners:push(Array.new(page,k,listener.event))
		end
	end
end
function Stage:dispatchEvent(evt)
	evt.target=self
	for i=0,self.stageListeners.length-1 do
		local a=self.stageListeners[i]
		local element=a[0]
		if not element:getStage() then
			self.stageListeners:splice(i,1)
			i=i-1
			goto next
		end
		if a[2]==evt.type then
			local method=element[a[1]]
			if method then
				method(element,evt)
			end
		end
		::next::
	end
end
function Stage:setEnabled(v)
	self.enabled=v
	if self.cover==nil then
		self.cover=self:addChild(Box.new())
	end
	self.cover.enabled=self.enabled
	self.cover:setEnabled(not self.enabled)
end


return Stage