local Motion=class()
function Motion:ctor(owner,func,...)
	self.step=0
	self.frame=0
	self.frameMax=0
	self.from=0
	self.to=0
	
	local args={...}
	args[0]=owner
	table.insert(args,1,func)
	table.insert(args,#args+1,self)
	App.timerId=App.timerId+1
	self.timerId=App.timerId
	App.timeQueues:push({timerId=App.timerId,delay=1,foo=args,startTime=os.clock()*1000,index=1,loop=true})
end
function Motion:stop()
	App.clearTimer(self.timerId)
	return self
end
function Motion:nextFrame()
	self.frame=self.frame+1
end
function Motion:nextStep()
	self.frame=0
	self.frameMax=0
	self.step=self.step+1
end
return Motion