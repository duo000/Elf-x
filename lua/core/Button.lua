local Button=class(Element)
function Button:ctor(symbol,style)
	self.className="Button"
	ClipData.setDefaultGrid(self.symbol,1,2)
	self.clip=self:addChild(Clip.new(self.symbol,self.style))
	self.paramIndex=0
	self.buttonIndex=0
	self.index=0
	self.text=nil
	local w=-1
	local h=-1
	if self.style.buttonIndex~=nil then self.buttonIndex=App.toInt(self.style.buttonIndex) end
	if self.style.width~=nil then w=App.toInt(self.style.width) end
	if self.style.height~=nil then h=App.toInt(self.style.height) end
	if self.style.paramIndex~=nil then self.paramIndex=self.style.paramIndex end
	if self.style.text~=nil then self.style.param=self.style.text end
	if self.style.param~=nil then
		local str=self.style.param
		if (String.slice(str,0,1)=="[") and (String.slice(str,-1)=="]") then
			self.params=String.split(String.slice(str,1,-1),",")
			str=self.params[Math.min(self.paramIndex,self.params.length-1)]
		end
		self.text=str
	end
	if self.style.pressPos~=nil then self.pressPos=App.formatArray(self.style.pressPos) end
	if self.style.disabledPos~=nil then self.disabledPos=App.formatArray(self.style.disabledPos) end
	if self.style.correctPos~=nil then self.correctPos=App.formatArray(self.style.correctPos) end
	if self.style.colors~=nil then self.colors=App.formatArray(self.style.colors) end
	if self.style.margins~=nil then self.margins=App.formatArray(self.style.margins) end
	if self.style.labelPadding~=nil then self.labelPadding=self.style.labelPadding end
	
	if self.colors==nil then self.colors=Array.new(0xffffff) end
	if self.margins==nil then self.margins=Array.new() end
	if self.correctPos==nil then self.correctPos=Array.new(0,0) end
	if self.pressPos==nil then self.pressPos=Array.new(0,3) end
	if self.disabledPos==nil then self.disabledPos=Array.new(0,0) end
	
	if w==-1 then w=self.clip.width end
	if h==-1 then h=self.clip.height end
	self.baseW=self.clip.width
	
	self:reset(self.buttonIndex,self.index,self.text,w,h)
end
function Button:reset(buttonIndex,index,text,w,h)
	self.buttonIndex=buttonIndex
	self.index=index
	self.text=text
	self.width=w
	self.height=h
	local colNum=self.clip.colNum
	local rowNum=self.clip.rowNum
	self.clip:reset(self.symbol,self.index*colNum+self.buttonIndex,w,h)
	if self.text==nil or self.text=="" then
		if self.label then self.label:setText("") end
		return
	end
	if self.label==nil then
		self.label=self:addChild(Label.new(nil,self.style))
		self.label:setSize(0,0)
	end
	self.label:setText(self.text)
	local m=self.margins
	if self.labelPadding then
		local w=Math.max(self.label.textWidth+4+((m[0]~=nil) and (m[0]+self.labelPadding) or (2*self.labelPadding)),self.baseW)
		self.clip:setWidth(w)
	end
	local x=m[0]~=nil and m[0] or Math.ceil((self.clip.width-self.label.textWidth)/2)+Math.floor(self.correctPos[0])
	local y=m[1]~=nil and m[1] or Math.ceil((self.clip.height-self.label.textHeight)/2)+Math.floor(self.correctPos[1])
	self.label:setPosition(x,y)
	self.label:setColor(self:getArrayAutoItem(self.colors,index))
	local x=(index==1) and (self.pressPos[0]) or (index==2 and self.disabledPos[0] or 0);
	local y=(index==1) and (-self.pressPos[1]) or (index==2 and -self.disabledPos[1] or 0);
	self.label.ccLabel["setPosition"](self.label.ccLabel,x,y)
	if self.label.ccNode2 then
		self.label.ccNode2["setPosition"](self.label.ccNode2,x,y)
	end
end
function Button:getArrayAutoItem(arr,index)
	while (arr[index]==nil) and (index>0) do
		index=index-1
	end
	return arr[index]
end
function Button:setSize(w,h)
	self:reset(self.buttonIndex,self.index,self.text,w,h)
end
function Button:setButtonIndex(v)
	self:reset(v,self.index,self.text,self.width,self.height)
end
function Button:setIndex(v)
	self:reset(self.buttonIndex,v,self.text,self.width,self.height)
end
function Button:setText(v)
	self:reset(self.buttonIndex,self.index,v,self.width,self.height)
end
function Button:onTouchEvent(evtType,x,y)
	if not App.stage.enabled then return end
	if evtType=="began" then
		self:setIndex(1)
		self:dispatchEvent({type="press",x=x,y=y})
	elseif evtType=="moved" then
		self:dispatchEvent({type="move",x=x,y=y})
	elseif evtType=="ended" then
		self:setIndex(0)
		self:dispatchEvent({type="release",x=x,y=y})
		if self:hitTest(x,y) then
			self:dispatchEvent({type="click",x=x,y=y})
			self:dispatchEvent({type="buttonClick"})
		end
	end
end

return Button