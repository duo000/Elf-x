local Math={}
Math.PI=_G["math"]["pi"]
Math.min=_G["math"]["min"]
Math.max=_G["math"]["max"]
Math.random=_G["math"]["random"]
Math.floor=_G["math"]["floor"]
Math.ceil=_G["math"]["ceil"]
Math.abs=_G["math"]["abs"]
Math.sqrt=_G["math"]["sqrt"]
Math.pow=_G["math"]["pow"]
Math.sin=_G["math"]["sin"]
Math.cos=_G["math"]["cos"]
Math.asin=_G["math"]["asin"]
Math.acos=_G["math"]["acos"]
Math.tan=_G["math"]["tan"]
Math.atan=_G["math"]["atan"]
return Math