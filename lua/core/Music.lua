local Music={}

function Music.play(url)
	App.audioEngine["playBackgroundMusic"](App.audioEngine,"assets/music/"..url)
end
function Music.stop()
	App.audioEngine["stopBackgroundMusic"](App.audioEngine)
end
function Music.pause()
	App.audioEngine["pauseBackgroundMusic"](App.audioEngine)
end
function Music.resume()
	App.audioEngine["resumeBackgroundMusic"](App.audioEngine)
end
function Music.isPlaying()
	return App.audioEngine["isBackgroundMusicPlaying"](App.audioEngine)
end
function Music.playSound(url)
	App.audioEngine["playEffect"](App.audioEngine,"assets/sound/"..url)
end

return Music