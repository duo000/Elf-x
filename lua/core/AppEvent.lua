local AppEvent=class(nil,{
	PRESS="press",
	RELEASE="release",
	CLICK="click",
	MOVE="move",
	BUTTON_CLICK="buttonClick",
	ENTER_FRAME="enterFrame",
	RESPONSE="response"
})

function AppEvent:ctor(type)
	self.type=type
end
return AppEvent
