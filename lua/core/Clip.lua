local Clip=class(Element)
function Clip:ctor(symbol,style)
	self.className="Clip"
	self.ccSprites=Array.new()
	self.flip=false
	self.useTempTexture=false
	local index=0
	local w=-1
	local h=-1
	if self.style.clipSize~=nil then self.clipSize=App.formatArray(self.style.clipSize) end
	if self.style.clip9Grid~=nil then self.clip9Grid=App.formatArray(self.style.clip9Grid) end
	if self.style.param~=nil then index=App.toInt(self.style.param) end
	if self.style.index~=nil then index=App.toInt(self.style.index) end
	if self.style.width~=nil then w=App.toInt(self.style.width) end
	if self.style.height~=nil then h=App.toInt(self.style.height) end
	if self.style.flip~=nil then self.flip=self.style.flip end
	if self.style.scrollRect~=nil then self.scrollRect=App.formatArray(self.style.scrollRect) end
	if self.style.centerPos~=nil then
		self.centerPos=App.formatArray(self.style.centerPos)
		if symbol==nil then
			self:setPosition(self.x+self.centerPos[0],self.y+self.centerPos[1])
		end
	end
	if self.style.useTempTexture~=nil then self.useTempTexture=self.style.useTempTexture end
	self:reset(self.symbol,index,w,h);
end
function Clip:reset(symbol,index,w,h)
	self:resetClip(symbol,index,w,h)
end
function Clip:resetClip(symbol,index,w,h)
	if symbol==nil then return end
	if index==nil then index=0 end
	if w==nil then w=-1 end
	if h==nil then h=-1 end
	self.index=index
	self.cropMode=App.cropItems[symbol..".1"]~=nil
	if App.cropItems[symbol..".1001"]~=nil then
		self.cropMode=true
		if self.index==0 then self.index=1000 end
	end
	if self.cropMode==false and App.cropItems[symbol.."."..(index+1)]~=nil then
		self.cropMode=true
	end
	if (self.symbol~=symbol) or (self.texture==nil) or self.cropMode then
		self.symbol=symbol
		self._symbol=self.symbol
		if self.cropMode then self._symbol=self._symbol.."."..(self.index+1) end
		self.texture=ClipData.getTexture(self._symbol,self.useTempTexure)
		self.baseW=self.texture["getPixelsWide"](self.texture)
		self.baseH=self.texture["getPixelsHigh"](self.texture)
		self.clipW=self.baseW
		self.clipH=self.baseH
		if self.clipSize~=nil then
			if _G["type"](self.clipSize[0])=="table" then
				local a=self.clipSize[0]
				self.clipSize[0]=self.baseW/a[0]
				self.clipSize[1]=self.baseH/a[1]
			end
			self.clipW=self.clipSize[0]
			self.clipH=self.clipSize[1]
		end
		self.colNum=self.baseW/self.clipW
		self.rowNum=self.baseH/self.clipH
	end
	local stateX=(index%self.colNum)*self.clipW
	local stateY=Math.floor(index/self.colNum)*self.clipH
	local g=self.clip9Grid
	if g==nil then
		if self.numChildren>1 then
			self.ccSprites:splice(0)
			self:removeChildren()
		end
		self.ccSprite=self.ccSprites[0]
		if self.ccSprite==nil then
			self.ccSprite=_G["CCSprite"]["create"](_G["CCSprite"])
			self.ccSprite.isBitmap=true
			self.ccSprites[0]=self.ccSprite
			self.ccNode["addChild"](self.ccNode,self.ccSprite)
			self.ccSprite["setAnchorPoint"](self.ccSprite,CCPoint(0,1))
		end
		self.ccSprite["setFlipX"](self.ccSprite,self.flip)
		local p=App.cropItems[self._symbol]
		local x=self.centerPos and -self.centerPos[0] or 0
		local y=self.centerPos and -self.centerPos[1] or 0
		if p==nil then
			self.ccSprite["setPosition"](self.ccSprite,x,-y)
			self.width=self.clipW
			self.height=self.clipH
		else
			if not self.flip then
				x=x+p[1]-p[3]/2
			else
				x=-x+p[1]-p[3]/2
				x=x-(self.clipW+2*x);
			end
			y=y+p[2]-p[4]/2
			if self.centerPos then
				x=x+p[3]/2
				y=y+p[4]/2
			end
			self.ccSprite["setPosition"](self.ccSprite,x,-y)
			stateX=0
			stateY=0
			self.width=p[3]
			self.height=p[4]
		end
		self.ccSprite["setTexture"](self.ccSprite,self.texture)
		w=self.clipW
		h=self.clipH
		if self.scrollRect then
			stateX=stateX+self.scrollRect[0]
			stateY=stateY+self.scrollRect[1]
			w=Math.min(self.scrollRect[2],w-self.scrollRect[0])
			h=Math.min(self.scrollRect[3],h-self.scrollRect[1])
		end
		if self.index==-1 then
			w=0
			h=0
			self.width=0
			self.height=0
		end
		self.ccSprite["setTextureRect"](self.ccSprite,CCRect(stateX,stateY,w,h))
		return
	end
	local w0=self.clipW
	local h0=self.clipH
	if g.length<4 then g:push(0,0,0) end
	w=w>-1 and w or w0
	h=h>-1 and h or h0
	if (g[4]==1) and (w>w0) then w=w0 end
	if (g[4]==1) and (h>h0) then h=h0 end
	self.width=w
	self.height=h
	if self.index==-1 then
		self.width=0
		self.height=0
	end
	local arr=Array.new(
		Array.new(0,0,g[0],g[2],						0,0,g[0],g[2]),
		Array.new(g[0],0,w0-g[0]-g[1],g[2],				g[0],0,w-g[0]-g[1],g[2]),
		Array.new(w0-g[1],0,g[1],g[2],					w-g[1],0,g[1],g[2]),
		
		Array.new(0,g[2],g[0],h0-g[2]-g[3],				0,g[2],g[0],h-g[2]-g[3]),
		Array.new(g[0],g[2],w0-g[0]-g[1],h0-g[2]-g[3],	g[0],g[2],w-g[0]-g[1],h-g[2]-g[3]),
		Array.new(w0-g[1],g[2],g[1],h0-g[2]-g[3],		w-g[1],g[2],g[1],h-g[2]-g[3]),
		
		Array.new(0,h0-g[3],g[0],g[3],					0,h-g[3],g[0],g[3]),
		Array.new(g[0],h0-g[3],w0-g[0]-g[1],g[3],		g[0],h-g[3],w-g[0]-g[1],g[3]),
		Array.new(w0-g[1],h0-g[3],g[1],g[3],			w-g[1],h-g[3],g[1],g[3])
	)
	local spriteIndex=0
	for i=0,8 do
		local a=arr[i]
		local isCrop=g[4]==1
		if (w<=g[0]) and (i%3==2) then a[6]=0 end
		if (w<=g[0]) and (i%3==0) then
			a[6]=w;
			isCrop=true;
		end
		if (w>g[0]) and (w<g[0]+g[1]) and (i%3==2) then
			a[4]=g[0];
			a[6]=w-g[0];
			isCrop=false
		end
		if (h<=g[2]) and (Math.floor(i/3)==2) then a[7]=0 end
		if (h<=g[2]) and (Math.floor(i/3)==0) then
			a[7]=h
			isCrop=true
		end
		if (h>g[2]) and (h<g[2]+g[3]) and (Math.floor(i/3)==2) then
			a[5]=g[2];
			a[7]=h-g[2];
			isCrop=false;
		end
		if (a[6]<=0) or (a[7]<=0) then goto next end
		local ccSprite=self.ccSprites[spriteIndex]
		if ccSprite==nil then
			ccSprite=_G["CCSprite"]["create"](_G["CCSprite"])
			ccSprite.isBitmap=true
			self.ccSprites[spriteIndex]=ccSprite
			self.ccNode["addChild"](self.ccNode,ccSprite)
			ccSprite["setAnchorPoint"](ccSprite,CCPoint(0,1))
		end
		local ccArr=self.ccNode["getChildren"](self.ccNode)
		ccSprite=self.ccSprites[spriteIndex]
		ccSprite["setTexture"](ccSprite,self.texture)
		ccSprite["setPosition"](ccSprite,a[4],-a[5])
		if self.index==-1 then
			a[6]=0
			a[2]=0
		end
		if isCrop then
			ccSprite["setTextureRect"](ccSprite,CCRect(stateX+a[0],stateY+a[1],a[6],a[7]))
			ccSprite["setScaleX"](ccSprite,1)
			ccSprite["setScaleY"](ccSprite,1)
		else
			ccSprite["setTextureRect"](ccSprite,CCRect(stateX+a[0],stateY+a[1],a[2],a[3]))
			ccSprite["setScaleX"](ccSprite,a[6]/a[2])
			ccSprite["setScaleY"](ccSprite,a[7]/a[3])
		end
		spriteIndex=spriteIndex+1
		::next::
	end
	self.ccSprites.length=spriteIndex
end
function Clip:setSize(w,h)
	self:reset(self.symbol,self.index,w,h)
end
function Clip:setIndex(v)
	self:reset(self.symbol,v,self.width,self.height)
end
function Clip:setCenterPos(x,y)
	self.centerPos=Array.new(x,y)
	self:setIndex(self.index)
end
function Clip:setScrollRect(x,y,w,h)
	self.scrollRect=Array.new(x,y,w,h)
	self:setIndex(self.index)
end
function Clip:setFlip(v)
	self.flip=v
	self:setIndex(self.index)
end
function Clip:setColor(v)
	for i=0,self.ccSprites.length do
		local ccSprite=self.ccSprites[i]
		ccSprite["setColor"](ccSprite,App.formatColor(v))
	end
end
function Clip:setParam(v)
	self:setIndex(App.toInt(v))
end
return Clip