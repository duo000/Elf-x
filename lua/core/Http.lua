local Http={}
function Http.send(data)
	local request=CCHTTPRequest:createWithUrlLua(Http.echo,data,kCCHTTPRequestMethodGET)
	request:start()
end
function Http.echo(evt)
	trace(evt.request:getResponseString())
end
return Http