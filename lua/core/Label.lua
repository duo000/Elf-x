local Label=class(Element)
function Label:ctor(symbol,style)
	self.className="Label"
	self.font="SimHei"
	self.size=36
	self.color=0xffffff
	self.align="left"
	self.leading=4
	self.letterSpacing=0
	self.wordWrap=false
	self.textWidth=0
	self.textHeight=0
	self.text=""
	self.ccSprites=Array.new()
	self.ccLabel=_G["CCLabelTTF"]["create"](_G["CCLabelTTF"],"",self.font,self.size)
	self.ccLabel["setAnchorPoint"](self.ccLabel,CCPoint(0,1))
	self.ccNode["addChild"](self.ccNode,self.ccLabel)
	self.ccLabel["setPosition"](self.ccLabel,2,-2)
	if self.style.font then self:setFont(self.style.font) end
	if self.style.size then self:setFontSize(self.style.size) end
	if self.style.color then self:setColor(self.style.color) end
	if self.style.align then self.align=self.style.align end
	if self.style.leading~=nil then self.leading=self.style.leading end
	if self.style.letterSpacing~=nil then self.letterSpacing=self.style.letterSpacing end
	if self.style.wordWrap~=nil then self.wordWrap=self.style.wordWrap end
	local w=0
	local h=0
	if self.style.width then w=Math.floor(self.style.width) end
	if self.style.height then h=Math.floor(self.style.height) end
	self:setSize(w,h)
	self:setAlign(self.align)
	if self.style.param then self:setText(self.style.param) end
	if self.style.text then self:setText(self.style.text) end
end
function Label:setFont(v)
	self.font=v
	self.ccLabel["setFontName"](self.ccLabel,v)
end
function Label:setFontSize(v)
	self.size=v
	self.ccLabel["setFontSize"](self.ccLabel,v)
end
function Label:setColor(v)
	self.color=v
	self.ccLabel["setColor"](self.ccLabel,App.formatColor(v))
end
function Label:setAlign(v)
	self.align=v
	local n=0
	if v=="center" or v=="centerMiddle" then n=1 end
	if v=="right" then n=2 end
	self.ccLabel["setHorizontalAlignment"](self.ccLabel,n)
end
function Label:setLeading(v)
	self.leading=v
end
function Label:setLetterSpacing(v)
	self.letterSpacing=v
end
function Label:setText(v)
	self.text=v
	local text=String.replace(self.text,"\\","\n")
 	if String.indexOf(self.font,"%$")~=0 then
		local w=0
		if self.wordWrap or self.align=="center" or self.align=="right" or self.align=="centerMiddle" then
			w=self.width
		end
		self.ccLabel["setDimensions"](self.ccLabel,CCSize(w,0))
		self.ccLabel["setString"](self.ccLabel,text)
		local size=self.ccLabel["getContentSize"](self.ccLabel)
		self.textWidth=size.width
		self.textHeight=size.height
		local y=0
		if self.align=="middle" or self.align=="centerMiddle" then
			y=(self.height-self.textHeight)/2
		end
		self.ccLabel["setPosition"](self.ccLabel,0,-y)
		return
	end
	if self.ccNode2==nil then
		self.ccNode2=_G["CCNode"]["create"](_G["CCNode"])
		self.ccNode["addChild"](self.ccNode,self.ccNode2)
	end
	local bfSymbol="lang.bmpFont."..String.slice(self.font,1)
	local index=0
	local x=0
	local y=0
	local maxCharH=0
    for c in _G["string"]["gfind"](text, "[%z\1-\127\194-\244][\128-\191]*") do
		if c=="\\" then
			x=0
			y=y+self.size+self.leading
			goto next
		end
		if c == " " or c=="　" then
			x=x+self.size+self.letterSpacing;
			goto next
		end
		local texture=ClipData.getTexture(bfSymbol.."_"..self:getCharHex(c))
        if texture==nil then goto next end
		local ccSprite=self.ccSprites[index]
		if ccSprite==nil then
			ccSprite=_G["CCSprite"]["create"](_G["CCSprite"])
			self.ccSprites:push(ccSprite)
			self.ccNode2["addChild"](self.ccNode2,ccSprite)
			ccSprite["setAnchorPoint"](ccSprite,CCPoint(0,1))
		end
		local charW=texture["getPixelsWide"](texture)
		local charH=texture["getPixelsHigh"](texture)
		ccSprite["setTexture"](ccSprite,texture)
		ccSprite["setPosition"](ccSprite,x,-y)
		ccSprite["setTextureRect"](ccSprite,CCRect(0,0,charW,charH))
		maxCharH=Math.max(maxCharH,charH)
		x=x+charW+self.letterSpacing
		if self.wordWrap and x+charW>=self.width then
			x=0
			y=y+maxCharH+self.leading
			maxCharH=0
		end
		index=index+1
		::next::
    end
	self.textWidth=x
	self.textHeight=y+maxCharH
	while self.ccSprites.length>index do
		ccSprite=self.ccSprites:pop()
		self.ccNode2:removeChild(ccSprite,true)
	end
	x=0
	y=0
	local w0=self.width
	local h0=self.height
	if self.align=="right" then
		x=w0-self.textWidth
	elseif self.align=="center" or self.align=="centerMiddle" then
		x=(w0-self.textHeight)/2
	end
	if self.align=="middle" or self.align=="centerMiddle" then
		y=(h0-self.textHeight)/2
	end
	self.ccNode2["setPosition"](self.ccNode2,x,-y)
end
function Label:getCharHex(c)
	local a={_G["string"]["byte"](c,1,#c)}
	local str=""
	for i=1,#a do
		str=str.._G["string"]["format"]("%x",a[i])
	end
	return "#"..str
end
function Label:setSize(w,h)
	self.width=w
	self.height=h
	self.ccLabel["setDimensions"](self.ccLabel,CCSize(w,h))
end
function Label:setParam(v)
	self:setText(v)
end

return Label