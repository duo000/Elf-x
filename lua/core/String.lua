local String={}
function String.length(str)
	if _G["type"](str)~="string" then return str.length end
	local _,count = _G["string"]["gsub"](str, "[^\128-\193]", "")
	return count
end
function String.slice(str,b,e)
	if _G["type"](str)~="string" then return str.slice(str,b,e) end
	local count=String.length(str)
	if b==nil then b=0 end
	if e==nil then e=count end
	if b<0 then b=Math.max(count+b,0) end
	if e<0 then e=Math.max(count+e,0) end
    local n=0
	local retStr=""
    for c in _G["string"]["gfind"](str, "[%z\1-\127\194-\244][\128-\191]*") do
        if n>=e then break end
        if n>=b then retStr=retStr..c end
		n=n+1
    end
	return retStr
end
function String.indexOf(str,s,i)
	if _G["type"](str)~="string" then return str.indexOf(str,s,i) end
	if i==nil then i=0 end
	if i<0 then i=Math.max(String.length(str)+i,0) end
	if i>0 then
		str=String.slice(str,i)
	end
	local n=_G["string"]["find"](str,s)
	if n==nil then return -1 end
	return String.length(_G["string"]["sub"](str,1,n))+i-1
end
function String.lastIndexOf(str,s,i)
	if _G["type"](str)~="string" then return str.lastIndexOf(str,s,i) end
	if s=="." then s="%." end
	if i==nil then i=0 end
	local n=-1
	repeat
		i=String.indexOf(str,s,i)
		if i>-1 then n=i end
		i=i+1
	until i<=0
	return n
end
function String.charAt(str,i)
    local n=0
    for c in _G["string"]["gfind"](str, "[%z\1-\127\194-\244][\128-\191]*") do
        if n==i then return c end
        n=n+1
    end
    return ""
end
function String.split(str,s)
	if _G["type"](str)~="string" then return str.split(str,s,i) end
	local arr=Array.new()
	local n=0
	repeat
		local b=n
		n=String.indexOf(str,s,b)
		if n==-1 then n=#str end
		arr:push(String.slice(str,b,n))
		n=n+1
	until n>=#str
	return arr
end
function String.replace(str,s,r)
	if (s==".") or (s=="[") or (s=="]") then
		s="%"..s
	end
	return _G["string"]["gsub"](str,s,r)..""
end
function String.toLowerCase(str)
	return _G["string"]["lower"](str)
end
function String.toUpperCase(str)
	return _G["string"]["upper"](str)
end
return String