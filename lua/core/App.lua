local App={}
function App.init()
	App.styles=_G["FlameUI"] or {}
	App.cropItems=_G["FlameCrop"] or {}
	App.tempTextures=Array.new()
	App.timeQueues=Array.new()
	App.baseW=960
	App.baseH=640
	local ccDirector=_G["CCDirector"]["sharedDirector"](_G["CCDirector"])
	local size=ccDirector["getWinSize"](ccDirector)
	App.width=size.width
	App.height=size.height
	App.timerId=0
	App.pressX=0
	App.pressY=0
	App.moveX=0
	App.moveY=0
	App.isPress=false
	App.touchIndex=0
	
	App.audioEngine=_G["SimpleAudioEngine"]["sharedEngine"](_G["SimpleAudioEngine"])
	App.userDefault=_G["CCUserDefault"]["sharedUserDefault"](_G["CCUserDefault"])
	
	App.stage=Stage.new()
	ccDirector["runWithScene"](ccDirector,App.stage.ccNode)
	App.stage.y=-math.max(App.height,App.baseH)
	App.stage.ccNode["setPosition"](App.stage.ccNode,0,-App.stage.y)
	App.root=Main.new()
	ccDirector["setProjection"](ccDirector,kCCDirectorProjection2D)
	local scheduler=ccDirector["getScheduler"](ccDirector)
	scheduler["scheduleScriptFunc"](scheduler,App.onTimer,0.01,false)
	App.stage:addChild(App.root)
	App.root:start()
end
function App.setPage(page)
	page:setEnabled(true)
	App.root:removeChildren()
	App.root:addChild(page)
end
function App.isClass(instance,classObj)
	if not instance.prototypes then return end
	local count=#instance.prototypes
	for i=1,count do
		if instance.prototypes[i]==classObj then return true end
	end
	return false
end
function App.setShareData(f,v)
	App.userDefault["setStringForKey"](App.userDefault,f,v)
end
function App.getShareData(f)
	return App.userDefault["getStringForKey"](App.userDefault,f)
end
function App.removeTempTexture()
	local textureCache=_G["CCTextureCache"]["sharedTextureCache"](_G["CCTextureCache"])
	for i=0,App.tempTextures.length-1 do
		local texture=App.tempTextures[i]
		textureCache["removeTexture"](textureCache,texture)
	end
	App.tempTextures:splice(0)
end
function App.bindStyle(symbol,f,v)
	if not App.styles[symbol] then App.styles[symbol]={} end
	App.styles[symbol][f]=v
end
function App.toInt(str)
	local n=_G["tonumber"](str) or 0
	if n>=0 then return Math.floor(n) end
	return Math.ceil(n)
end
function App.doTimeout(owner,func,delay,...)
	local args={...}
	args[0]=owner and owner or func
	if owner then _G["table"]["insert"](args,1,func) end
	App.timerId=App.timerId+1
	App.timeQueues:push({timerId=App.timerId,delay=delay or 1,foo=args,startTime=getTimer(),index=1})
	return App.timerId
end
function App.doInterval(owner,func,delay,...)
	local args={...}
	args[0]=owner and owner or func
	if owner then _G["table"]["insert"](args,1,func) end
	App.timerId=App.timerId+1
	App.timeQueues:push({timerId=App.timerId,delay=delay or 1,foo=args,startTime=getTimer(),index=1,loop=true})
	return App.timerId
end
function App.exec(foo)
	if not foo then return end
	if _G["type"](foo[0])=="function" then
		foo[0](foo[1],foo[2],foo[3],foo[4],foo[5])
	elseif _G["type"](foo[1])=="function" then
		foo[1](foo[0],foo[2],foo[3],foo[4],foo[5],foo[6])
	end
end
function App.eval(code)
	return _G["loadstring"](code)()
end
function App.formatColor(color)
	return _G["ccc3"](Math.floor(color/(256*256)),Math.floor(color%(256*256)/256),color%(256*256*256))
end
function App.formatArray(arr)
	if _G["type"](arr)~="string" then return arr end
	if arr=="" then return nil end
	if String.slice(arr,0,1)~="[" then arr="["..arr.."]" end
	arr=String.replace(String.replace(arr,"[","Array.new("),"]",")")
	arr=_G["loadstring"]("return "..arr)()
	return arr
end
function App.onTimer()
	local currTime=getTimer()
	local i=0
	while i<App.timeQueues.length do
		local queue=App.timeQueues[i]
		if (currTime-queue.startTime>=queue.delay*queue.index) then
			if(queue.loop==nil) then
				App.timeQueues:splice(i,1)
				i=i-1
			end
			queue.index=Math.floor((currTime-queue.startTime)/queue.delay)+1
			local n=App.timeQueues.length
			App.exec(queue.foo)
			if n>App.timeQueues.length then i=i-1 end
		end
		i=i+1
	end
	App.stage:dispatchEvent({type="enterFrame"})
end
function App.clearTimer(timerId)
	for i=0,App.timeQueues.length-1,1 do
		local queue=App.timeQueues[i]
		if (queue.timerId==timerId) then
			App.timeQueues:splice(i,1)
			break;
		end
	end
end
return App