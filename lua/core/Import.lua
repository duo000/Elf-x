undefined=false
function import(path)
	local className=_G["string"]["gsub"](path,"%a+%.", "")
	if _G[className] == nil then
		_G[className]=_G["require"](path)
	end
end
function _(obj)
	return obj
end
function translate(str)
	return str
end
function trace(...)
	local arg = {...}
	local str=""
	if #arg==0 then str="nil" end
	for i=1,#arg do
		if (_G["type"](arg[i])=="table") and (arg[i].toString~=nil) then
			str=str..arg[i]:toString()
		else
			str=str.._G["tostring"](arg[i])
		end
		if i<#arg then str=str.." " end
	end
	_G["CCLuaLog"](str)
end
function getTimer()
	return _G["os"]["clock"]()*1000
end
