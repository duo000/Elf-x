local _class={}
local function class(super,static_data)
	local class_type={}

	class_type.ctor=false
	class_type.super=super
	
	class_type.new=function(...) 
			local owner={}
			owner.prototypes={class_type}

			if static_data and static_data.eventListener then
				owner.eventListener=static_data.eventListener
				if rawget(_G, "App") and App.stage then
					App.stage:addPageListener(owner)
				end
			end

			setmetatable(owner,{ __index=_class[class_type] })
	        owner.super = _class[super]
			
			do
				local create
				create = function(c,...)
					if c.super then
						table.insert(owner.prototypes,c.super)
						create(c.super,...)
					end
					if owner._define then
						owner:_define()
					end
					if c.ctor then
						c.ctor(owner,...)
					end
				end
				create(class_type,...)
			end
			
	
			return owner
	end
	
	local vtbl={}

	if static_data and _G["type"](static_data)=="table" then
		for i,v in pairs(static_data) do
			class_type[i] = v
		end
	end
		
	_class[class_type]=vtbl
	
	setmetatable(class_type,{__newindex=
		function(t,k,v)
			vtbl[k]=v
		end
	})
	class_type.class_prop=function(p)
		return vtbl[p]
	end

	
	if super then
		setmetatable(vtbl,{__index=
			function(t,k)
				local ret=_class[super][k]
				vtbl[k]=ret
				return ret
			end
		})
	end

	return class_type
end
return class
