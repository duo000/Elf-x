local FlameUI={
	["components.dlgBg1"]={clip9Grid="60,60,60,30"},
	["components.TextInput"]={clip9Grid="9,9,9,9"},
	["world.Animate_footShining"]={clipSize="120,80"},
	["components.Button"]={size="42",letterSpacing="10",font="$button"},
	["components.cover"]={clip9Grid="0"},
	["components.blank"]={clip9Grid="0"},
	["components.Label"]={clip9Grid="9,9,9,9",size="20",font="SimHei"}
}
return FlameUI