local Ease=class(nil,{eventListener={},none=true,strongIn=true,strongOut=true,strongInOut=true,backIn=true,backOut=true,backInOut=true,circIn=true,circOut=true,circInOut=true,elasticIn=true,elasticOut=true,elasticInOut=true,quadIn=true,quadOut=true,quadInOut=true})
function Ease.none(t,b,c,d)
	if (true) then
		return c*t/d+b
	end
end
function Ease.strongIn(t,b,c,d)
	t=t/d
	if (true) then
		return c*t*t*t*t*t+b
	end
end
function Ease.strongOut(t,b,c,d)
	t=t/d-1
	if (true) then
		return c*(t*t*t*t*t+1)+b
	end
end
function Ease.strongInOut(t,b,c,d)
	t=t/d/2
	if (t<1) then
		return c/2*t*t*t*t*t+b
	end
	t=t-2
	if (true) then
		return c/2*(t*t*t*t*t+2)+b
	end
end
function Ease.backIn(t,b,c,d,s)
	if s==nil then s=1.70158 end
	t=t/d
	if (true) then
		return c*t*t*((s+1)*t-s)+b
	end
end
function Ease.backOut(t,b,c,d,s)
	if s==nil then s=1.70158 end
	t=t/d-1
	if (true) then
		return c*(t*t*((s+1)*t+s)+1)+b
	end
end
function Ease.backInOut(t,b,c,d,s)
	if s==nil then s=1.70158 end
	t=t/d/2
	if (t<1) then
		s=s*(1.525)
		if (true) then
			return c/2*(t*t*((s+1)*t-s))+b
		end
	end
	t=t-2
	s=s*(1.525)
	if (true) then
		return c/2*(t*t*((s+1)*t+s)+2)+b
	end
end
function Ease.circIn(t,b,c,d)
	t=t/d
	if (true) then
		return-c*(Math.sqrt(1-t*t)-1)+b
	end
end
function Ease.circOut(t,b,c,d)
	t=t/d-1
	if (true) then
		return c*Math.sqrt(1-t*t)+b
	end
end
function Ease.circInOut(t,b,c,d)
	t=t/d/2
	if (t<1) then
		return-c/2*(Math.sqrt(1-t*t)-1)+b
	end
	t=t-2
	if (true) then
		return c/2*(Math.sqrt(1-t*t)+1)+b
	end
end
function Ease.elasticIn(t,b,c,d,a,p)
	if a==nil then a=0 end
	if p==nil then p=0 end
	local s
	if (t==0) then
		return b
	end
	t=t/d
	if (t==1) then
		return b+c
	end
	if (not p) then
		p=d*.3
	end
	if (not a or a<Math.abs(c)) then
		a=c
		s=p/4
	else
		s=p/(Math.PI*2)*Math.asin(c/a)
	end
	t=t-1
	if (true) then
		return-(a*Math.pow(2,10*t)*Math.sin((t*d-s)*Math.PI*2/p))+b
	end
end
function Ease.elasticOut(t,b,c,d,a,p)
	if a==nil then a=0 end
	if p==nil then p=0 end
	local s
	if (t==0) then
		return b
	end
	t=t/d
	if (t==1) then
		return b+c
	end
	if (not p) then
		p=d*0.3
	end
	if (not a or a<Math.abs(c)) then
		a=c
		s=p/4
	else
		s=p/(Math.PI*2)*Math.asin(c/a)
	end
	if (true) then
		return(a*Math.pow(2,-10*t)*Math.sin((t*d-s)*Math.PI*2/p)+c+b)
	end
end
function Ease.elasticInOut(t,b,c,d,a,p)
	if a==nil then a=0 end
	if p==nil then p=0 end
	local s
	if (t==0) then
		return b
	end
	t=t/d/2
	if (t==2) then
		return b+c
	end
	if (not p) then
		p=d*(.3*1.5)
	end
	if (not a or a<Math.abs(c)) then
		a=c
		s=p/4
	else
		s=p/(Math.PI*2)*Math.asin(c/a)
	end
	if (t<1) then
		t=t-1
		if (true) then
			return-0.5*(a*Math.pow(2,10*t)*Math.sin((t*d-s)*Math.PI*2/p))+b
		end
	end
	t=t-1
	if (true) then
		return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*Math.PI*2/p)*0.5+c+b
	end
end
function Ease.quadIn(t,b,c,d)
	t=t/d
	if (true) then
		return c*t*t+b
	end
end
function Ease.quadOut(t,b,c,d)
	t=t/d
	if (true) then
		return-c*t*(t-2)+b
	end
end
function Ease.quadInOut(t,b,c,d)
	t=t/d*0.5
	if (t<1) then
		return c*0.5*t*t+b
	end
	t=t-1
	if (true) then
		return-c*0.5*(t*(t-2)-1)+b
	end
end
return Ease