﻿{
	"components.Button":{"font":"$button","letterSpacing":"10","size":"42"},
	"components.Label":{"clip9Grid":"9,9,9,9","font":"SimHei","size":"20"},
	"components.TextInput":{"clip9Grid":"9,9,9,9"},
	"components.blank":{"clip9Grid":"0"},
	"components.cover":{"clip9Grid":"0"},
	"components.dlgBg1":{"clip9Grid":"60,60,60,30"},
	"world.Animate_footShining":{"clipSize":"120,80"}
}